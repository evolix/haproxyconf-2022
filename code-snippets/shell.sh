/usr/sbin/varnishd […] -a /run/varnish.sock,PROXY […]
----
/usr/sbin/varnishd […] -a 127.0.0.1:82 […]
----
curl --verbose \
     --resolve www.example.com:82:127.0.0.1 \
     --header "X-Forwarded-Proto: https" \
     http://www.example.com:82/foo/bar
----
+X@Ike1sspdiNAko5YHK9HAAAAC4|GET /blog/ HTTP/1.1|user-agent:curl/7.64.0|accept:*/*|host:jeremy.lecour.fr|x-forwarded-for:1.2.3.4, 4,5,6,7|accept-encoding:gzip|x-varnish:65545|x-forwarded-port:443|x-forwarded-proto:http|connection:close
-X@Ike1sspdiNAko5YHK9HAAAAC4
----
