sub vcl_recv {
    # HAProxy check
    if (req.url == "/varnishcheck") {
        return(synth(200, "Hi HAProxy, I'm fine!"));
    }
    […]
}
----
backend default {
    .path = "/run/haproxy-frontend-default.sock";
    .proxy_header = 1;
    […]
}
----
sub vcl_recv {
    […]
    set req.http.X-Boost-Step2 = "varnish";
}
----
sub vcl_deliver {
    […]
    if (resp.http.Set-Cookie && resp.http.Cache-Control) {
      set resp.http.X-Boost-Step2 = "varnish WITH set-cookie AND cache-control on backend server";
    } elseif (resp.http.Set-Cookie) {
      set resp.http.X-Boost-Step2 = "varnish WITH set-cookie and NO cache-control on backend server";
    } elseif (resp.http.Cache-Control) {
      set resp.http.X-Boost-Step2 = "varnish with NO set-cookie and WITH cache-control on backend server";
    } else {
      set resp.http.X-Boost-Step2 = "varnish with NO set-cookie and NO cache-control on backend server";
    }
----